# Contributor Code of Conduct

## Be friendly and patient

We understand that everyone has different levels of experience or knowledge in many diverse fields, be it technical or non-technical in nature.
We also have areas of knowledge we are eager to expand.
We want to be a company where people can not only produce high quality codebase, but feel comfortable to ask questions as well and learn along the way.
If someone says something wrong, or says something accidentally offensive, respond with patience and try to keep it polite and civil.
Remember that we all were newbies at one point.

## Be welcoming

We strive to be a company that welcomes and supports people of all backgrounds and identities.
This includes, but is not limited to, members of any race, ethnicity, culture, national origin, color, immigration status, social and economic class,
educational level, sex, sexual orientation, gender identity and expression, age, size, family status, political belief,
religion, and mental and physical ability.

## Be considerate

Your work will be used by other people, and you in turn will depend on the work of others.
Any decision you make will affect users and colleagues, and you should take those consequences into account when making decisions.

## Be respectful

Not all of us will agree all the time, but disagreement is no excuse for poor behavior and poor manners.
We might all experience some frustration now and then, but we cannot allow that frustration to turn into a personal attack.
It’s important to remember that a community where people feel uncomfortable or threatened is not a productive one.
Members of Navocap should be respectful when dealing with other members as well as with people outside the Navocap development team.

## Be careful in the words that you choose

We are a company of professionals, and we conduct ourselves professionally. Be kind to others. Do not insult or put
down other participants. Harassment and other exclusionary behavior aren’t acceptable. This includes, but is not limited to:

- Violent threats or language directed against another person
- Discriminatory jokes and language
- Posting sexually explicit or violent material
- Posting (or threatening to post) other people’s personally identifying information (“doxing”)
- Personal insults, especially those using racist or sexist terms
- Unwelcome sexual attention
- Advocating for, or encouraging, any of the above behavior
- Repeated harassment of others. In general, if someone asks you to stop, then stop

## When we disagree, try to understand why

Disagreements, both social and technical, happen all the time and Navocap projects are no exception.
It is important that we resolve disagreements and differing views constructively.
Remember that we’re different.
The strength of Navocap comes from its varied team, people from a wide range of backgrounds.
Different people have different perspectives on issues.
Being unable to understand why someone holds a viewpoint doesn’t mean that they’re wrong.
Don’t forget that it is human to err and blaming each other doesn’t get us anywhere.
Instead, focus on helping to resolve issues and learning from mistakes.

Original text courtesy of the Speak Up! project and Django Project.