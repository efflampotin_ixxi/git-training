# git training

## Introduction

Hi there,

Here you will find a lecture about git basics.

It is written in markdown using [marp](https://marp.app/) presentation framework.

## Getting started

[npx (`npm exec`)](https://docs.npmjs.com/cli/v7/commands/npx) is the best way to use the latest Marp CLI if you wanted
one-shot Markdown conversion _without install_. Just run below if you have
installed [Node.js](https://nodejs.org/) 12 and later.

If you want to convert the markdown into **HTML, PDF or PowerPoint** presentation,
please use the following commands:

```bash
# Convert slide deck into HTML
npx @marp-team/marp-cli@latest docs/git-in-a-nutshell.md
npx @marp-team/marp-cli@latest docs/git-in-a-nutshell.md -o output.html

# Convert slide deck into PDF
npx @marp-team/marp-cli@latest docs/git-in-a-nutshell.md --pdf
npx @marp-team/marp-cli@latest docs/git-in-a-nutshell.md -o output.pdf

# Convert slide deck into PowerPoint document (PPTX)
npx @marp-team/marp-cli@latest docs/git-in-a-nutshell.md --pptx
npx @marp-team/marp-cli@latest docs/git-in-a-nutshell.md -o output.pptx

```

That's all folks! 
