---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---
<style>
  :root {
    --color-highlight: #05AB8F;
  }
</style>

![bg left:40% 80%](https://jiraratpsmartsystems.atlassian.net/s/-dtzt95/b/6/138460ec47518567ecfa3e164e05b62b/_/jira-logo-scaled.png)

# **Git in a nutshell**

Efflam POTIN - Architect

---

# what is git

**[git](https://git-scm.com/)** is a **source control system** that is:

* blazing **performant**
* pretty **lightweight**
* **easy to learn**
* **distributed** (even if your workflow is not)

It is the **standard de facto** when it comes to scm.

---

# why do I need git?

* When you need to **version your work** (in other words pretty much **every time** you develop something)
* Versioning is important for **coordinating work** among multiple people on a project
* It is useful to **track progress**
* To **understand the changes over time** on a project
* Used as a **single source of truth**, your git repository handle your tech documentation, diagrams, build and deploy pipelines, etc...

---

# what are Gitlab or Github?

Gitlab is **not** git

**Gitlab**, **Github**, **Bitbucket**, etc... are commercial solutions to **host** your git repositories.

Depending on the provider you choose, you will have access to different flavour of services like:

* devops tools and continuous integration and deployment pipelines
* third-party integrations (Jira, Zendesk,etc...)
* project management (tickets, milestone, time tracking), etc...

---

# why do we use Gitlab?

At the moment of writing, **RATP Smart Systems** is using **Gitlab.com** as our **git server**

Using the cloud version of Gitlab means that every  hosting detail is managed directly by the Gitlab company. So we can **focus on building valuable code** instead of struggling with Gitlab monthly updates

It ensures **high availability** of our git-based tools and ensures that it is **always up-to-date**

However, our **gitlab-ci runners** are hosted on prem (thru k8s :rocket:)

---

# create your first git repository

In order to keep things simple, we will create our first git repository from gitlab

>:warning: Before you go further, make sure you
> have already created a Gitlab account

:pushpin: Just click **[here](https://gitlab.com/projects/new#blank_project)**

As it is just a playground, please create it in your **personal space** 🙏

For this example, we will name our git repository **git-example**

---

# install git

Before we go further make sure you've installed git on your local machine. If not, just grab it [here](https://git-scm.com/)

Or use your favorite package manager :package:

```bash
apt-get install git # for debian-based linux
brew install git # for MacOS using Homebrew 
choco install git # for Windows using Chocolatey
```

Also, make sure your ssh keys are correctly set up in order to interact with the git server, see [here](https://docs.gitlab.com/ee/ssh/)

---

# git clone

Now that you've created the repository on gitlab, you may want to **contribute** to its code

To do so, you will need to **clone** your git repository in local

Type the following lines in your favorite terminal (:warning: replace the username):

```bash
cd directory/to/your/repo
git clone git@gitlab.com:username/git-example.git
cd git-example
```

---

# git clone: what does it do ?

This will download a **.git** repository from the internet (Gitlab) to your computer and extract the latest snapshot of the repo (all the files) to your working directory

 By default it will all be saved in a folder with the same name as the repo (in this case **git-example**)

:sparkles: The URL you specify here is called the remote origin (the place where the files were originally downloaded from).

This term will be used later on.

---

# git status

```bash
git status
```

This will print some basic information, such as which files have recently been modified

You should check your status anytime you’re confused

Git will print additional information depending on what’s currently going on in order to help you out

---

# create a branch

```bash
git branch <new-branch-name>
```

or

```bash
git checkout -b <new-branch-name>
```

In order to make sure that your new branch is based on a specific you type like this:

```bash
git checkout -b <new-branch-name> origin/main # new-branch-name will be based on origin/main
```

---

# create a branch: explanations

You can think of this like creating a local “checkpoint” (technically called a reference) and giving it a name

It’s similar to doing File > Save as… in a text editor; the new branch that gets created is a reference to the current state of your repo

The branch name can then be used in various other commands as you’ll soon see.

Similar to branching, more commonly you will save each checkpoint as you go along in the form of commits

---

# create a branch: explanations (suite)

Commits are a particular type of checkpoint called a revision

The name will be a random-looking hash of numbers and letters such as **e093542**

This hash can then be used in various other commands just like branch names.

That’s really the core function of git:

To save checkpoints (revisions) and share them with other people. Everything revolves around this concept

---

# create a branch: explanations (suite 2)

If you’ve ever created a checkpoint to something, you’ll be able to get back to it later as long as your **.git** folder is intact

It’s magical :tada:

See **[git reflog](https://git-scm.com/docs/git-reflog)** if you’re interested in learning more.

---

# view the differences between checkpoints

```bash
git diff <branch-name> <other-branch-name>
```

After editing some files, you can simply type git diff to view a list of the changes you’ve made

This is a good way to double-check your work before committing it

For each group of changes, you’ll see what the file used to look like (prefixed with **-** in red), and what it looks like now (prefixed with **+** in green).

---

# stage your changes

```bash
git add <files>
```

After editing some files, this command will mark any changes you’ve made as “staged” (or “ready to be committed”)

⚠️ If you then go and make more changes, those new changes will not automatically be staged, even if you’ve changed the same files as before

This is useful for controlling exactly what you commit, but also a major source of confusion for newcomers.

---

# stage your changes: explanations

If you’re ever unsure, just type git status again to see what’s going on

You’ll see “Changes to be committed:” followed by file names in green

Below that you’ll see “Changes not staged for commit:” followed by file names in red. These are not yet staged

As a shortcut, you can use wildcards just like with any other terminal command. For example:

```bash
git add README.md app/*.txt
```

---

# stage your changes: explanations (suite)

```bash
git add README.md app/*.txt
```

This will add the file README.md, as well as every file in the **app** folder that ends in .txt. Typically you can just type **git add --all** to add everything that’s changed

---

# git commit

```bash
git commit -m "<commit message>"
```

This will save your commit locally

The commit message is important to help other people understand what was changed and why you changed it

Please use [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#commit-message-with-multi-paragraph-body-and-multiple-footers) in order to write useful commit messages

---

# a typical **good** commit message

```git
fix: prevent racing of requests

Introduce a request id and a reference to latest request. Dismiss incoming responses other than from latest request.

Remove timeouts which were used to mitigate the racing issue but are
obsolete now.
```

See also:

<https://web.microsoftstream.com/video/b80f0ce6-2a8f-48da-8ccc-0a2209ecfba9>

<https://web.microsoftstream.com/video/1027b1d9-3caa-426b-9a2d-11e7b21146ba>

---

# :boom: atomic commit

When we talk about atomic commits as a best practice we mean every commit should be atomic in terms of features present in your app

Every commit should be related to one feature only.

Do not make a commit which is related to even two features

The reason is simple: It makes keeping track of the relevant changes difficult

After a while if a bug occurs then it will be difficult for everyone, you included, to track down where things went wrong

---

# :boom: atomic commit (suite)

Different features really belong to different git branches not in the same commits.

Non-atomic commits **may break** the ability to apply a **[git bisect](https://git-scm.com/docs/git-bisect)**

Non-atomic commits can make **difficult to revert a commit**

Non-atomic commits are :shit:

---

# git rebase interactive

When it comes to keep your git history as **human-understandable** and keep your commits **atomics**, you will dig the **git rebase interactive command**

```bash
git rebase -i HEAD~<number-of-commits-you-want-deal-with>
```

In order to dive in this topic, please click [here](https://medium.com/fredwong-it/git-rebase-how-to-use-interactive-rebase-properly-34db370be995)

---

# push your branch

```bash
git push origin <branch-name>
```

This will upload your branch to the **remote** named **origin** (remember, that’s the URL defined initially during clone)

After a successful push, your teammates will then be able to pull your branch to view your commits

---

# push your branch (suite)

😎 As a shortcut, you can type the word **HEAD** instead of **branch-name** to automatically use the branch you’re currently on

**HEAD** always refers to your latest checkpoint, that is, the latest commit on your current branch.

---

# push force your branch

```bash
git push --force-with-lease origin <branch-name>
```

If you need to overwrite a remote branch with your local branch, you can use **--force** option

**--force-with-lease** is a safer option that will not overwrite any work on the remote branch if more commits were added to the remote branch (by another team-member or coworker or what have you)

It ensures you do not overwrite someone else's work by force pushing

---

# push force your branch (suite)

It is a good practice to git push your work each day to the git server

It ensures that even if your local machine is broken down, your work is saved

---

# fetch the latest news

```bash
git fetch --prune
```

This will download the latest info about the repo from **origin** (such as all the different branches stored on Gitlab)

It doesn’t change any of your local files — just updates the tracking data stored in the *.git* folder

If you want your local files to be updated, you need to:

```bash
git rebase
```

---

# fetch the latest news (suite)

```bash
git fetch --prune
```

This will download the latest info about the repo from **origin** (such as all the different branches stored on Gitlab)

It doesn’t change any of your local files — just updates the tracking data stored in the *.git* folder

If you want your local files to be updated, you need to:

```bash
git rebase
```

---

# code review

Once you pushed your brand new branch on the git server,
it is a very good practice to **make sure that someone else review it**

In order to that, we can take advantage of **Gitlab** and its **merge request** concept

**Remember:** always create a merge request as soon as your work is ready to be shared and reviewed

Also, it is good practice to ensure that two people, at least, review each branch

---

# merge your code

By leveraging Gitlab and its merge request, you can directly merge your branch into the target branch from the Gitlab UI (if it has been tested and approved)

If you need to merge a branch from your terminal:

```bash
git merge <new-feature-branch>
```

---

# git workflows

There are two main workflows when it comes to git:

* Git Flow
* Github Flow

Both have a **master/main** branch that represents the production state

In Git Flow, there is a **develop** branch that represents the pre-production state

---

# git workflows (suite)

In both cases, new feature development are isolated into dedicated **feature branch** (ex: feat/<jira-ticket>-add-scheduled-interconnections)

And in both cases, bug fixings are isolated into dedicated **hotfix branch** (ex: hotfix/<jira-ticket>-fix)

:warning: Please never merge main branch into your feature branch.

**Prefer to fetch+rebase your feature branch on the latest main branch**
